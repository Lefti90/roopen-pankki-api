####################
# Build ts into js #
####################

FROM alpine:latest as builder

RUN apk add --update nodejs npm
WORKDIR /build
COPY ./src ./src
COPY ./package*.json ./
COPY ./tsconfig.json ./

RUN npm ci
RUN npm run build


######################
# Create final image #
######################

FROM alpine:latest as final

ARG APP_PORT
ARG PG_HOST
ARG PG_PORT
ARG PG_USERNAME
ARG PG_PASSWORD
ARG PG_DATABASE
ARG DEV_STATUS

ENV APP_PORT=${APP_PORT}
ENV PG_HOST=${PG_HOST}
ENV PG_PORT=${PG_PORT}
ENV PG_USERNAME=${PG_USERNAME}
ENV PG_PASSWORD=${PG_PASSWORD}
ENV PG_DATABASE=${PG_DATABASE}
ENV DEV_STATUS=${DEV_STATUS}

RUN apk add --update nodejs npm
WORKDIR /app
COPY --from=builder ./build/dist ./dist
COPY ./package*.json ./
RUN npm ci --omit=dev

EXPOSE $APP_PORT
CMD ["npm", "start"]