import { executeQuery } from "../db"
import * as queries from "./queries"

const depositToAccount = async (accountId: number, amount: number) => {
    const queryParams = [accountId, amount]
    const result = await executeQuery(queries.depositToAccount, queryParams)
    return result.rows
}

const withdrawFromAccount = async (accountId: number, amount: number) => {
    const queryParams = [accountId, amount]
    const result = await executeQuery(queries.withdrawFromAccount, queryParams)
    return result.rows
}

export default {
    depositToAccount,
    withdrawFromAccount,
}