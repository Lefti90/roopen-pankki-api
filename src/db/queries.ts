// /users queries
export const insertUser = "INSERT INTO users (username, password) VALUES ($1, $2);"

export const findAllUsers = "SELECT id, username FROM users;"

export const findOneUser = "SELECT * FROM users WHERE username = $1;"

export const depositToAccount = "UPDATE accounts SET balance = balance + $2 WHERE id = $1;"

export const withdrawFromAccount = "UPDATE accounts SET balance = balance - $2 WHERE id = $1;"
export const updateUsername = "UPDATE users SET username = $2 WHERE username = $1;"

export const updatePassword = "UPDATE users SET password = $2 WHERE password = $1;"
