import express, { Request, Response } from "express"
import accountsRouter from "./accountsRouter"


const server = express()
server.use(express.json())

//accounts router
server.use("/accounts", accountsRouter)

server.get("/", (_req: Request, res: Response) => {
    res.send("/GET works")
})

server.get("/version", (_req: Request, res: Response) => {
    res.send("/GET version 1.0")
})



export default server