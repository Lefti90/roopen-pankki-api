import { executeQuery } from "./db"

const mode = process.env.DEV_STATUS
let table = ""
if(mode === "dev"){
    table = "test_accounts"
}else{
    table = "accounts"
}

///////////CREATE ACCOUNT//////////////
// Endpoint: POST /accounts 
// Parameters: initial balance 
// Return value: account id, balance
const createAccount = async (balance: number, userID: number) => {
    //Check if max 3 accounts
    const query1 = `
    SELECT COUNT(id) FROM ${table}
    WHERE userid = ${userID};
    `
    const result = await executeQuery(query1)
    console.log(result.rows[0])
    if(result.rows[0].count >= 3){
        return "Error, too many accounts"
    }

    //Create account
    const query = `
        INSERT INTO ${table} (balance, userid)
	        VALUES (${balance}, ${userID});
    `

    //Select id and balance for return
    const query2 = `
    SELECT id, balance FROM ${table}
    WHERE userid = ${userID}
    ORDER BY id, userid DESC LIMIT 1;
    `
    await executeQuery(query)
    console.log("New account inserted succesfully.")
    const result2 = await executeQuery(query2)
    return result2.rows[0]
}

/////////GET users accounts//////////
// Endpoint: GET /accounts/user/:userId 
// Parameters: - Return 
// value: list of account id's
const getUserAccounts = async (userid: number) => {    
    const query = `
    SELECT id FROM ${table}
    WHERE userid = ${userid};
    `
    const result = await executeQuery(query)
    return result.rows
}

////////DELETE user accounts//////////////
// Endpoint: DELETE /accounts/:accountId 
// Parameters: - Return 
// value: -
const deleteUserAccount = async (accountid: number, userid: number) => {
    const query = `
    DELETE FROM ${table}
    WHERE userid = ${userid} AND id = ${accountid};
    `
    await executeQuery(query)
    return "-"
}

/////////GET account balance///////////////
// Endpoint: GET /accounts/:accountId 
// Parameters: - Return 
// value: balance
const getAccountBalance = async (accountid: number) => {
    const query = `
    SELECT balance FROM ${table}
    WHERE id = ${accountid};
    `
    const result = await executeQuery(query)
    console.log(result.rows)
    return result.rows
}

export default { 
    createAccount, 
    getUserAccounts, 
    deleteUserAccount, 
    getAccountBalance 
}