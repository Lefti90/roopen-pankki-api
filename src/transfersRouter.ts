import express, { Request, Response } from "express"
import transfersDao from "./db/transfersDao"
import accountsDao from "./accountsDao"

const router = express.Router()

//Deposit to
router.patch("/deposit", async (req: Request, res: Response) => {
    const { accountId, amount } = req.body
    await transfersDao.depositToAccount(accountId, amount)

    const result = await accountsDao.getAccountBalance(accountId)
    const balance = result[0]["balance"]
    res.status(201).send(balance)
})

//Withdraw from
router.patch("/withdraw", async (req: Request, res: Response) => {
    const { accountId, amount } = req. body

    const balanceCheckResult = await accountsDao.getAccountBalance(accountId)
    const balanceBefore = balanceCheckResult[0]["balance"]
    if (balanceBefore < amount) return res.status(400).send("Insufficient funds")

    transfersDao.withdrawFromAccount(accountId, amount)
    
    const result = await accountsDao.getAccountBalance(accountId)
    const balanceAfter = result[0]["balance"]
    res.status(201).send(balanceAfter)
})

//Transfer from-to
router.patch("/transfer", async (req: Request, res: Response) => {
    const { sourceAccountId, targetAccountId, amount } = req. body
    
    const sourceBalanceCheckResult = await accountsDao.getAccountBalance(sourceAccountId)
    const sourceBalanceBefore = sourceBalanceCheckResult[0]["balance"]
    if (sourceBalanceBefore < amount) return res.status(400).send("Insufficient funds on source account")

    await transfersDao.withdrawFromAccount(sourceAccountId, amount)
    await transfersDao.depositToAccount(targetAccountId, amount)

    const result = await accountsDao.getAccountBalance(sourceAccountId)
    const sourceBalanceAfter = result[0]["balance"]
    res.status(201).send(sourceBalanceAfter)
})

export default router
