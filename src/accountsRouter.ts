import { Router } from "express"
import dao from "./accountsDao"

const router = Router()
//Router to -> /accounts
router.get("/", (req, res) => {
    const body = req.body
    res.status(200).send(body)
})

//Create new account
router.post("/", async (req, res) => {
    const balance = req.body.balance
    const userid = req.body.userid
    const result = await dao.createAccount(balance, userid)
    res.status(200).send(result)
})

//Get user accounts
router.get("/user/:userid", async (req, res) => {
    const userid = Number(req.params.userid)
    const result = await dao.getUserAccounts(userid)
    res.status(200).send(result)
})

//Delete user account
router.delete("/:accountid", async (req, res) => {
    const accountid = Number(req.params.accountid)
    const userid = Number(req.body.userid)
    const result = await dao.deleteUserAccount(accountid, userid)
    res.status(200).send(result)
})

//Get account balance
router.get("/:accountid", async (req, res) => {
    const accountid = Number(req.params.accountid)
    const result = await dao.getAccountBalance(accountid)
    res.status(200).send(result)
})

export default router